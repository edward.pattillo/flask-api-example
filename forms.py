from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import ValidationError, DataRequired, EqualTo, Length


class GameName(FlaskForm):

    game_name = StringField('Game Name')

