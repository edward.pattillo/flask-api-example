from flask import Flask, render_template, request, redirect, url_for
# import our config file config.py
from config import Config
from datetime import datetime 
from forms import GameName

# need this to make API requests
import requests

# deal with data
import json
from csv import DictWriter, DictReader

# create an object with our Config class properties
config = Config()

# instantiate flask
app = Flask(__name__, static_url_path='/static')

# set the secret key for encryption
app.secret_key = config.secret_key




# ---------------------------------------------

# this route defines the behavior at the root endpoint of our site
@app.route("/")
def hello():

    form = GameName()
    # this reaches into our template folder and renders the home.html page
    return render_template("home.html",form=form)





# ---------------------------------------------

# this endpoint is not for public use. It's a utility for AJAX lookups
# It is called behind the scenes when the user makes a selection and presses enter
@app.route("/look", methods=['POST'])
def ajax_lookup():

    # treats the AJAX look up like any other user submitted POST request
    form = GameName()

    if form.validate_on_submit():

        user_input = request.form
        game_name = user_input['game_name'].strip() # removes white space from user input
        # print(game_name)

        # need to look up game ID in JSON file
        data = json.loads(open("static/games.json").read())

        # using the game name from user input, find the RAWG game ID so that we can 
        # look up the precise game from their API
        game_id = str(data[game_name])

        # go grab all the data in JSON format
        response = requests.get('https://api.rawg.io/api/games/'+game_id).json()
        # print("response:",response)

        # this returns the JSON result back to the page
        return response

    else:
        return json.dumps(form.errors)





# ---------------------------------------------
# this endpoint does the huge and time consuming task of creating a CSV file of 
# game names and IDs from the RAWG API. 
@app.route("/make_csv")
def make_csv():

    making = True
    page = 3600
    ordering = '-released'
    page_size = 40

    API_endpoint = 'https://api.rawg.io/api/games?ordering='+ordering+'&page='+str(page)+'&page_size='+str(page_size)

    while making == True:

        response = requests.get(API_endpoint)
        data = json.loads(response.text)

        # print("count:",data['count'])
        # print("previous:",data['previous'])
        print("next:",data['next'])

        if data['next'] == 'None':
            making = False
        else:
            API_endpoint = data['next']


        all_game_info = data['results']


        for game in all_game_info:

            y,m,d = game['released'].split('-')
            game_date = datetime(int(y),int(m),int(d))
            # print(game_date)]

            # only worry about games that have at least 4 ratings (otherwise it gets crazy)
            if game_date <= datetime.now() and int(game['ratings_count']) > 3:

                # create a little dictionary of game id and name
                this_game = {"id":game['id'],"name":game['name']}

                with open("static/games.csv", 'a+', encoding="utf-8", newline='') as write_obj:
                    # Create a writer object from csv module
                    dict_writer = DictWriter(write_obj, fieldnames=["id","name"])
                    # Add dictionary as wor in the csv
                    dict_writer.writerow(this_game)

    # this is just here because each endpoint needs to render something
    return "CSV file made."


# ---------------------------------------------
# this route is for converting our CSV file into a more useable JSON format
@app.route("/make_json")
def make_json():

    # open previously created CSV for reading
    csvfile = open('static/games.csv', 'r', encoding="utf8")
    # open json file for writing
    jsonfile = open('static/games.json', 'w', encoding="utf8")

    # copy contents of CSV file to a reader object 
    reader = DictReader(csvfile, delimiter=',')

    # declare empty dictionary
    outputDict = {}

    # iterate through contents of CSV and pull the bits out that we need
    for row in reader:

        id = int(row['id'])
        name = row['name'].replace('"','')

        # create a key/value pair that we can add to our dictionary
        this_pair = {name:id}
        outputDict.update(this_pair)
 
    
    # # remove indent param for slim JSON      , indent=4 
    # out = json.dumps(outputDict, indent=4 )
    out = json.dumps(outputDict)
    jsonfile.write(out)

    # this is just here because each endpoint needs to render something
    return "JSON File made."

# ---------------------------------------------






# finally, this runs our app
# to get rid of debug mode just set it to False or remove it

if __name__ == "__main__":
    app.run(debug=True)





 