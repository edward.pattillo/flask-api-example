// this lets the jQuery wait until the whole page has loaded
$(document).ready(function () {

    // declare empty array
    let game_name_list = [];

    // open JSON file and load contents (game info) into game_name_list
    $.getJSON("static/games.json", function (json) {

        // iterate through the JSON file and create a list of game names
        $.each(json, function (index, element) {
            // console.log(index)
            game_name_list.push(index);
        });
    });

    // console.log(game_name_list);

    // puts the cursor in the field (one less click for the user)
    $('#game_name').focus();

    // uses the jQuery UI library to render the auto-suggest menu under the field as the user types
    $('#game_name').autocomplete({
        classes: {
            "ui-autocomplete": "highlight"
        },
        source: game_name_list,
        minLength: 2
    });

    // when the user presses enter
    $('#game_form').on('submit', function(e) {
        // this keeps the form from actually loading a new page
        e.preventDefault();
        // grabe game name from field
        let game_name = $('#game_name').val();
        // now go grab the game the user has selected from the function that calls the API
        ajax_game(game_name);
    });

    // when user clicks on a game in the auto-suggest menu, it populates the field
    $('.ui-menu-item').click(function () {
        $('#game_name').val(this.text);
    });
 

    // goes and grabs game info from flask endpoint 'look'
    // it gets a nifty JSON object as a response
    function ajax_game() {

        $.ajax({
            type: "POST",
            url: '/look',
            dataType: "json",
            data: $('form').serialize(), // this takes the value in the form field and gets it ready to AJAX
            success: function (gameJSON) {

                console.log(gameJSON);

                // go to a function that makes the response look pretty
                renderGameInfo(gameJSON);

            },
            error: function (error) {
                console.log(error);
            }

        }).done(function () {
            // clear the game name field so use can look up another
            $('#game_name').val('');
        });
    }

    // this function takes the info from the API call and creates an HTML presentation
    function renderGameInfo(gameJSON) {

        console.log(gameJSON.name);

        // we're going to make one variable to hold the HTML and keep adding to it with +=s
        let game_html = '<div class="container mb-sm-5">';
        game_html += '<div class="row">';

        game_html += '<div class="col-4">';
        game_html += '<img src="'+gameJSON.background_image+'" class="game-img img-responsive img-rounded" />';
        game_html += '</div>';

        game_html += '<div class="col-4">';
        game_html += '<h4>'+gameJSON.name+'</h4>';
        game_html += '</div>';

        game_html += '<div class="col-4">';
        // only show this info if it exists in the JSON results
        if (gameJSON.esrb_rating) { game_html += '<p>ESRB rating: '+gameJSON.esrb_rating.name+'</p>';}
        if (gameJSON.rating) { game_html += '<p>RAWG rating: '+gameJSON.rating+'</p>';}
        if (gameJSON.metacritic) { game_html += '<p>Metacritic rating: '+gameJSON.metacritic+'</p>';}
        game_html += '</div>';

        game_html += '</div>'; // end of row
        game_html += '<div class="row">';

        game_html += '<div class="col-12">';
        game_html += '<p>'+gameJSON.description+'</p>';
        game_html += '</div>';

        game_html += '</div>'; // end of row

        game_html += '</div>'; // end of container

        // prepend our game info to the .show_game div
        $('.show_game').prepend(game_html);

    }


});