# Flask API Lookup Example

To demo this, open a CMD prompt and navigate to your folder of choice. Then use:

```git clone git@gitlab.com:edward.pattillo/flask-api-example.git```

Navigate into the app's root folder then recreate the virtual environment:

```pip install -r requirements.txt```

This should take a wee bit. When done, activate virtual environment:

```venv/scripts/activate```

Then just run the app in dev mode:

```py app.py```

You are free to use the bits from the API lookup code in your project, but you must leave a shoutout in your comments when you use it so that you're legit and acknowedging use of an open-source tool.

The code is properly commented. You should be able to figure out how it works. Please do this before you copy-pasta or else you won't be able to debug or modify it to suit.

You don't need to recreate the CSV file. This takes ages. Literally over 24 hours as it involved querying the massive RAWG database only 40 games at a time, then filtering through results. Just use the CSV file as a basis  for your work as it is a local and (hopefully) accurate data store.

You may want to tweek the contents of the JSON file yourself. I was going for minimal file size for faster user experience. The JSON only contains the name of the game and it's unique ID in the RAWG database for accuracy. It could be reduced to a smaller file size by only containing the game names which would be used to populate the jQuery UI autosuggest menu. You would then need to use something like the existing JSON file to look up key/value pairs to ascertain the ID for the game you want then do a lookup. It works fairly well at the moment though so I didn't bother.

Hope this helps. 

Arohanui,
Mr P
